MC_DEBUG = __DebugAdapter ~= nil
MC_FACTORIO2 = storage ~= nil

MC_LINES = 256
MC_OUTPUT = 256
MC_REGS = 8 -- used for GUI preview
MC_REGS_EXT = 64
MC_REGS_RO_FIRST = 1000
MC_REGS_RO_MSLOT = 2000
MC_MEMORY_CHANNELS = 4
MC_MEMORY_SLOTS_MIN = 8
MC_SAVESLOTS = 750 --51 -- see https://github.com/NiftyManiac/factorio-stickynotes/blob/master/config.lua
MC_BROWNOUT_LEVEL = 0.4
