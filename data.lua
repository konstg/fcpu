table = require('3rdparty.flib061.table')

require('src/constants')
require('prototypes/control')
require('prototypes/style')
require('prototypes/gui')
require('prototypes/item')
require('prototypes/signals')
require('prototypes/wiki')
