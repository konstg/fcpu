data:extend{
  {
      type = "virtual-signal",
      name = "signal-fcpu-halt",
      icon = "__fcpu__/graphics/icons/signal/signal_halt.png",
      icon_size = 64,
      subgroup = "virtual-signal",
      order = "f[fcpu]-[a]"
  },
  {
      type = "virtual-signal",
      name = "signal-fcpu-run",
      icon = "__fcpu__/graphics/icons/signal/signal_run.png",
      icon_size = 64,
      subgroup = "virtual-signal",
      order = "f[fcpu]-[b]"
  },
  {
      type = "virtual-signal",
      name = "signal-fcpu-step",
      icon = "__fcpu__/graphics/icons/signal/signal_step.png",
      icon_size = 64,
      subgroup = "virtual-signal",
      order = "f[fcpu]-[c]"
  },
  {
      type = "virtual-signal",
      name = "signal-fcpu-sleep",
      icon = "__fcpu__/graphics/icons/signal/signal_sleep.png",
      icon_size = 64,
      subgroup = "virtual-signal",
      order = "f[fcpu]-[d]"
  },
  {
      type = "virtual-signal",
      name = "signal-fcpu-jump",
      icon = "__fcpu__/graphics/icons/signal/signal_jump.png",
      icon_size = 64,
      subgroup = "virtual-signal",
      order = "f[fcpu]-[e]"
  },
  {
      type = "virtual-signal",
      name = "signal-fcpu-error",
      icon = "__fcpu__/graphics/icons/signal/signal_error.png",
      icon_size = 64,
      subgroup = "virtual-signal",
      order = "f[fcpu]-[f]"
  },
}
