data:extend{
  {
    type = "font",
    name = "fcpu-mono",
    from = "fcpu-mono",
    size = 14
  },
  {
    type = "font",
    name = "fcpu-mono-small",
    from = "fcpu-mono",
    size = 13
  },
}


data:extend{
  {
    type = "sprite",
    name = "fcpu-pin-black",
    filename = "__fcpu__/graphics/icons/gui/pin.png",
    priority = "extra-high-no-scale",
    size = 32,
    scale = 0.5,
    flags = {"gui-icon"}
  },
  {
    type = "sprite",
    name = "fcpu-pin-white",
    filename = "__fcpu__/graphics/icons/gui/pin.png",
    priority = "extra-high-no-scale",
    x = 32,
    y = 0,
    size = 32,
    scale = 0.5,
    flags = {"gui-icon"}
  },
}

data:extend{
  {
    type = "sprite",
    name = "fcpu-play-sprite",
    filename = "__fcpu__/graphics/icons/gui/play.png",
    width = 32,
    height = 32
  },
  {
    type = "sprite",
    name = "fcpu-stop-sprite",
    filename = "__fcpu__/graphics/icons/gui/stop.png",
    width = 32,
    height = 32
  },
  {
    type = "sprite",
    name = "fcpu-pause-sprite",
    filename = "__fcpu__/graphics/icons/gui/pause.png",
    width = 32,
    height = 32
  },
  {
    type = "sprite",
    name = "fcpu-next-sprite",
    filename = "__fcpu__/graphics/icons/gui/next.png",
    width = 32,
    height = 32
  },
  {
    type = "sprite",
    name = "fcpu-copy-sprite",
    filename = "__base__/graphics/icons/shortcut-toolbar/mip/copy-x24.png",
    width = 24,
    height = 24
  },
  {
    type = "sprite",
    name = "fcpu-paste-sprite",
    filename = "__base__/graphics/icons/shortcut-toolbar/mip/paste-x24.png",
    width = 24,
    height = 24
  },
  {
    type = "sprite",
    name = "fcpu-memory-sprite",
    filename = "__core__/graphics/icons/mip/grid-view.png",
    x = 0,
    y = 0,
    width = 32,
    height = 32
  },
}
